package com.mappers;

public interface Mapperable<T,E> {
    /**@param entity Klasa entiteta*/
    T mapToDTO(E entity);
    /**@param dto Klasa dto*/
    E mapToEntity(T dto);
}
