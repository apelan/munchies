package com.mappers;

import com.models.dto.ItemorderDTO;
import com.models.entities.Itemorder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ItemorderMapper implements Mapperable<ItemorderDTO, Itemorder> {

    @Autowired
    private GrouporderMapper grouporderMapper;

    @Override
    public ItemorderDTO mapToDTO(Itemorder entity) {
        ItemorderDTO dto = new ItemorderDTO();

        dto.setId(entity.getId());
        dto.setEmployeeName(entity.getEmployeeName());
        dto.setItemName(entity.getItemName());
        dto.setPrice(entity.getPrice());
        dto.setGroupOrder(grouporderMapper.mapToDTO(entity.getGroupOrder()));

        return dto;
    }

    @Override
    public Itemorder mapToEntity(ItemorderDTO dto) {
        Itemorder entity = new Itemorder();

        entity.setEmployeeName(dto.getEmployeeName());
        entity.setItemName(dto.getEmployeeName());
        entity.setPrice(dto.getPrice());

        if(entity.getGroupOrder() != null) {
            entity.setGroupOrder(grouporderMapper.mapToEntity(dto.getGroupOrder()));
        }

        return entity;
    }
}
