package com.mappers;

import com.models.dto.GrouporderDTO;
import com.models.dto.RestaurantDTO;
import com.models.entities.Grouporder;
import com.models.entities.Restaurant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RestaurantMapper implements Mapperable<RestaurantDTO, Restaurant> {

    @Autowired
    private GrouporderMapper grouporderMapper;

    @Override
    public RestaurantDTO mapToDTO(Restaurant entity) {
        List<GrouporderDTO> ordersDTO = new ArrayList<>();
        RestaurantDTO dto = new RestaurantDTO();

        if(entity.getGroupOrders() != null) {
            for (Grouporder go : entity.getGroupOrders()) {
                ordersDTO.add(grouporderMapper.mapToDTO(go));
            }
            dto.setGroupOrders(ordersDTO);
        }

        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setAddress(entity.getAddress());
        dto.setPhoneNumber(entity.getPhoneNumber());
        dto.setMenuLink(entity.getMenuLink());
        dto.setDeliveryInfo(entity.getDeliveryInfo());
        dto.setUuid(entity.getUuid());
        dto.setEmail(entity.getEmail());

        return dto;
    }

    @Override
    public Restaurant mapToEntity(RestaurantDTO dto) {
        Restaurant entity = new Restaurant();

        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setAddress(dto.getAddress());
        entity.setPhoneNumber(dto.getPhoneNumber());
        entity.setMenuLink(dto.getMenuLink());
        entity.setDeliveryInfo(dto.getDeliveryInfo());
        entity.setUuid(dto.getUuid());
        entity.setEmail(dto.getEmail());

        return entity;
    }
}
