package com.mappers;

import com.models.dto.UserDTO;
import com.models.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper implements Mapperable<UserDTO, User> {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public UserDTO mapToDTO(User entity) {
        UserDTO dto = new UserDTO();

        dto.setId(entity.getId());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        dto.setEmail(entity.getEmail());
        dto.setPassword(entity.getPassword());
        dto.setRole(roleMapper.mapToDTO(entity.getRole()));

        return dto;
    }

    @Override
    public User mapToEntity(UserDTO dto) {
        User entity = new User();

        entity.setId(dto.getId());
        entity.setFirstName(dto.getFirstName());
        entity.setLastName(dto.getLastName());
        entity.setEmail(dto.getEmail());
        entity.setPassword(dto.getPassword());
        entity.setRole(roleMapper.mapToEntity(dto.getRole()));

        return entity;
    }
}
