package com.mappers;

import com.models.dto.RoleDTO;
import com.models.entities.Role;
import org.springframework.stereotype.Component;


@Component
public class RoleMapper implements Mapperable<RoleDTO, Role> {

    @Override
    public RoleDTO mapToDTO(Role entity) {
        RoleDTO dto = new RoleDTO();

        dto.setId(entity.getId());
        dto.setName(entity.getName());

        return dto;
    }

    @Override
    public Role mapToEntity(RoleDTO dto) {
        Role entity = new Role();

        if(dto != null) {
            entity.setId(dto.getId());
            entity.setName(dto.getName());
        }

        return entity;
    }
}
