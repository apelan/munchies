package com.mappers;

import com.models.dto.GrouporderDTO;
import com.models.entities.Grouporder;
import org.springframework.stereotype.Component;

@Component
public class GrouporderMapper implements Mapperable<GrouporderDTO, Grouporder>{

    @Override
    public GrouporderDTO mapToDTO(Grouporder entity) {
        GrouporderDTO dto = new GrouporderDTO();

        dto.setId(entity.getId());
        dto.setCreator(entity.getCreator());
        dto.setTimeout(entity.getTimeout());
        dto.setTimeCreated(entity.getTimeCreated());
        dto.setUuid(entity.getUuid());
        dto.setSent(entity.isSent());

        return dto;
    }

    @Override
    public Grouporder mapToEntity(GrouporderDTO dto) {
        Grouporder entity = new Grouporder();

        entity.setId(dto.getId());
        entity.setCreator(dto.getCreator());
        entity.setTimeout(dto.getTimeout());
        entity.setTimeCreated(dto.getTimeCreated());
        entity.setUuid(dto.getUuid());
        entity.setSent(dto.isSent());

        return entity;
    }
}
