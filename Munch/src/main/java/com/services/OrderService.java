package com.services;

import com.models.dto.GrouporderDTO;
import com.models.dto.RestaurantDTO;

import java.util.List;

public interface OrderService {
    List<GrouporderDTO> findAllOrders();
    GrouporderDTO saveOrUpdate(GrouporderDTO order, RestaurantDTO restaurantDTO);
    GrouporderDTO findById(int id);
    GrouporderDTO findByUuid(String uuid);
    GrouporderDTO addGroupOrder(GrouporderDTO order, Integer id);
    double getTotalItemsSum(GrouporderDTO order);
    void deleteOrdersAndItems(List<GrouporderDTO> orders);
    List<GrouporderDTO> getActiveOrdersFromRestaurant(Integer id);
    void setEmailTimer(long delay, RestaurantDTO restaurantDTO, GrouporderDTO grouporderDTO);
}
