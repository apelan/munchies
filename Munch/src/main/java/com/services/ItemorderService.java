package com.services;

import com.models.dto.GrouporderDTO;
import com.models.dto.ItemorderDTO;
import com.models.entities.Itemorder;

import java.util.List;

public interface ItemorderService {
    List<ItemorderDTO> findAllItems();
    void saveOrUpdate(ItemorderDTO item);
    ItemorderDTO findItemById(int id);
    void deleteAllItems(Iterable<? extends Itemorder> i);
    List<ItemorderDTO> findAllItemsByGroupOrder(GrouporderDTO grouporderDTO);
    ItemorderDTO saveItemInGroup(ItemorderDTO item, Integer id);
    ItemorderDTO placeItemInGroup(ItemorderDTO item, Integer id);
}
