package com.services.implementation;

import com.mappers.ItemorderMapper;
import com.models.dto.GrouporderDTO;
import com.models.dto.ItemorderDTO;
import com.models.entities.Grouporder;
import com.models.entities.Itemorder;
import com.repositories.ItemorderRepository;
import com.repositories.OrderRepository;
import com.services.ItemorderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ItemorderServiceImpl implements ItemorderService {

    @Autowired
    private ItemorderRepository repo;

    @Autowired
    private ItemorderMapper mapper;

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public List<ItemorderDTO> findAllItems() {
        List<ItemorderDTO> dtos = new ArrayList<>();

        for(Itemorder i : repo.findAll()){
            dtos.add(mapper.mapToDTO(i));
        }

        return dtos;
    }

    @Override
    public void saveOrUpdate(ItemorderDTO item) {
        repo.save(mapper.mapToEntity(item));
    }

    @Override
    public ItemorderDTO findItemById(int id) {
        return mapper.mapToDTO(repo.findById(id).get());
    }

    @Override
    public void deleteAllItems(Iterable<? extends Itemorder> i) {
        repo.deleteAll(i);
    }

    @Override
    public List<ItemorderDTO> findAllItemsByGroupOrder(GrouporderDTO grouporderDTO) {

        List<ItemorderDTO> allDTOItems = new ArrayList<>();
        List<ItemorderDTO> allDTOItemsFromGroupOrder = new ArrayList<>();

        for(Itemorder i : repo.findAll()){
            allDTOItems.add(mapper.mapToDTO(i));
        }

        for(ItemorderDTO i : allDTOItems){
            if(i.getGroupOrder().getId() == grouporderDTO.getId()){
                allDTOItemsFromGroupOrder.add(i);
            }
        }

        return allDTOItemsFromGroupOrder;
    }

    @Override
    public ItemorderDTO saveItemInGroup(ItemorderDTO item, Integer id) {
        Itemorder i = mapper.mapToEntity(item);
        Grouporder go = orderRepository.getOne(id);

        i.setGroupOrder(go);
        repo.save(i);

        return mapper.mapToDTO(i);
    }

    @Override
    public ItemorderDTO placeItemInGroup(ItemorderDTO item, Integer id) {
        Itemorder i = mapper.mapToEntity(item);
        Grouporder go = orderRepository.getOne(id);

        i.setGroupOrder(go);
        return mapper.mapToDTO(i);
    }
}
