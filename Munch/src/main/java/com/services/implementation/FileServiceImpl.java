package com.services.implementation;

import com.models.dto.RestaurantDTO;
import com.services.fileUpload.FileService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class FileServiceImpl implements FileService {

    @Value("${menu.path}")
    private String menuPath;

    @Override
    public RestaurantDTO uploadFile(MultipartFile multipartFile, RestaurantDTO restaurantDTO) {
        String fileName = restaurantDTO.restaurantShortName() + "_" + multipartFile.getOriginalFilename();

        if(!multipartFile.getOriginalFilename().trim().equals("")){
            try {
                byte[] bytes = multipartFile.getBytes();
                Path path = Paths.get(menuPath + fileName);
                Files.write(path, bytes);
                restaurantDTO.setMenuLink(fileName);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            restaurantDTO.setMenuLink("noMenuImg.jpg");
        }

        return restaurantDTO;
    }

    @Override
    public void deleteFile(String fileName) {
        try {
            if(!fileName.equals("noMenuImg.jpg")) {
                Path path = Paths.get(menuPath + fileName);
                Files.deleteIfExists(path);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getFileExtension(Path path) {
        int lastIndexOf = path.toString().lastIndexOf(".");

        String extension = path.toString().substring(lastIndexOf);
        System.out.println(extension);

        switch(extension){
            case ".jpg":
                return "image/jpeg";
            case ".png":
                return "image/png";
            case ".gif":
                return "image/gif";
            case ".pdf":
                return "application/pdf";
            case ".txt":
                return "text/plain";
            case ".docx":
                return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            case ".doc":
                return "application/msword";
            default:
                return "application/octet-stream";
        }
    }
}
