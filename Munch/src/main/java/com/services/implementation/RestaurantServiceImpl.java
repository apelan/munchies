package com.services.implementation;

import com.mappers.RestaurantMapper;
import com.models.dto.RestaurantDTO;
import com.models.entities.Restaurant;
import com.repositories.RestaurantRepository;
import com.services.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class RestaurantServiceImpl implements RestaurantService {

    @Autowired
    private RestaurantRepository repo;

    @Autowired
    private RestaurantMapper mapper;

    @Override
    public RestaurantDTO saveOrUpdate(RestaurantDTO restaurant) {
        if(restaurant.getUuid() == null){
            restaurant.setUuid(UUID.randomUUID().toString());
        }
        return mapper.mapToDTO(repo.save(mapper.mapToEntity(restaurant)));
    }

    @Override
    public void deleteRestaurantById(Integer id) {
        repo.deleteById(id);
    }

    @Override
    public void deleteRestaurantByUuid(String uuid) {
        deleteRestaurantById(findRestaurantByUuid(uuid).getId());
    }

    @Override
    public List<RestaurantDTO> findAllRestaurants() {
        List<RestaurantDTO> dtos = new ArrayList<>();

        for(Restaurant r : repo.findAll()){
            dtos.add(mapper.mapToDTO(r));
        }

        return dtos;
    }

    @Override
    public RestaurantDTO findRestaurantById(Integer id) {
        return mapper.mapToDTO(repo.findById(id).get());
    }

    @Override
    public RestaurantDTO findRestaurantByUuid(String uuid) {
        return mapper.mapToDTO(repo.findByUuid(uuid));
    }

    @Override
    public RestaurantDTO getOne(Integer id) {
        return mapper.mapToDTO(repo.getOne(id));
    }

    @Override
    public RestaurantDTO findByMenuLink(String menuLink) {
        return mapper.mapToDTO(repo.findByMenuLink(menuLink));
    }
}
