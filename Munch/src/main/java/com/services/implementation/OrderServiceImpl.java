package com.services.implementation;

import com.mappers.GrouporderMapper;
import com.mappers.RestaurantMapper;
import com.models.dto.GrouporderDTO;
import com.models.dto.ItemorderDTO;
import com.models.dto.RestaurantDTO;
import com.models.entities.Grouporder;
import com.repositories.OrderRepository;
import com.services.ItemorderService;
import com.services.MailService;
import com.services.OrderService;
import com.services.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository repo;

    @Autowired
    private ItemorderService itemService;

    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private GrouporderMapper mapper;

    @Autowired
    private RestaurantMapper restaurantMapper;

    @Autowired
    private MailService mailService;

    @Override
    public List<GrouporderDTO> findAllOrders() {
        List<GrouporderDTO> dtos = new ArrayList<>();

        for(Grouporder go : repo.findAll()){
            dtos.add(mapper.mapToDTO(go));
        }

        return dtos;
    }


    @Override
    public GrouporderDTO findById(int id) {
        return mapper.mapToDTO(repo.findById(id).get());
    }

    @Override
    public GrouporderDTO findByUuid(String uuid) {
        return mapper.mapToDTO(repo.findByUuid(uuid));
    }

    @Override
    public GrouporderDTO saveOrUpdate(GrouporderDTO order, RestaurantDTO restaurantDTO) {
        Grouporder goEntity = mapper.mapToEntity(order);
        goEntity.setRestaurant(restaurantMapper.mapToEntity(restaurantDTO));

        repo.save(goEntity);

        return mapper.mapToDTO(goEntity);
    }

    @Override
    public GrouporderDTO addGroupOrder(GrouporderDTO orderDTO, Integer id) {
        Grouporder orderEntity = mapper.mapToEntity(orderDTO);
        orderEntity.setTimeCreated(new Timestamp(System.currentTimeMillis()));
        orderEntity.setUuid(UUID.randomUUID().toString());
        orderEntity.setRestaurant(restaurantMapper.mapToEntity(restaurantService.getOne(id)));
        orderEntity.setSent(false);
        repo.save(orderEntity);

        return mapper.mapToDTO(orderEntity);
    }

    @Override
    public void setEmailTimer(long delay, RestaurantDTO restaurantDTO, GrouporderDTO grouporderDTO){
        mailService.emailTimer(delay, restaurantDTO, grouporderDTO);
    }

    @Override
    public double getTotalItemsSum(GrouporderDTO order) {
        double sum = 0;

        for(ItemorderDTO i : itemService.findAllItems()){
            if(i.getGroupOrder().getId() == order.getId()){
                sum += i.getPrice();
            }
        }

        return sum;
    }

    @Override
    public void deleteOrdersAndItems(List<GrouporderDTO> orders) {
        List<Grouporder> grouporders = new ArrayList<>();

        for(GrouporderDTO go : orders){
            grouporders.add(mapper.mapToEntity(go));
        }

        for(Grouporder go : grouporders){
            itemService.deleteAllItems(go.getItemOrders());
            repo.delete(go);
        }
    }

    @Override
    public List<GrouporderDTO> getActiveOrdersFromRestaurant(Integer id){
        RestaurantDTO r = restaurantService.findRestaurantById(id);
        List<GrouporderDTO> allFromRestaurantDTO = r.getGroupOrders();

        List<GrouporderDTO> orders = new ArrayList<>();

        for(GrouporderDTO o : allFromRestaurantDTO) {
            long expireDate = o.getTimeCreated().getTime() + (Long.parseLong(o.getTimeout()) * 60 * 1000);

            //current < expire = not expired
            //current > expire = expired
            if(System.currentTimeMillis() < expireDate){
                orders.add(o);
            }

        }
        return orders;
    }

}
