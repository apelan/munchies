package com.services.implementation;

import com.config.WebMVCConfig;
import com.mappers.UserMapper;
import com.models.dto.RoleDTO;
import com.models.dto.UserDTO;
import com.models.entities.User;
import com.repositories.UserRepository;
import com.services.RoleService;
import com.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repo;

    @Autowired
    private UserMapper mapper;

    @Autowired
    private RoleService roleService;

    @Override
    public UserDTO findUserByEmail(String email) {
        return mapper.mapToDTO(repo.findByEmail(email));
    }

    @Override
    public List<UserDTO> findAllUsers() {
        List<UserDTO> dtos = new ArrayList<>();

        for(User u :  repo.findAll()){
            dtos.add(mapper.mapToDTO(u));
        }

        return dtos;
    }

    @Override
    public UserDTO saveOrUpdate(UserDTO u) {
        RoleDTO role = roleService.findByName("admin");
        u.setRole(role);

        User user = mapper.mapToEntity(u);

        user.setPassword(WebMVCConfig.passwordEncoder().encode(user.getPassword()));

        user = repo.save(user);
        return mapper.mapToDTO(user);
    }

    @Override
    public void deleteUserById(Integer id) {
        repo.deleteById(id);
    }

    @Override
    public boolean checkIfPasswordMatches(String password, String confirmPassword) {
        return password.equals(confirmPassword);
    }

}
