package com.services.implementation;

import com.mappers.RoleMapper;
import com.models.dto.RoleDTO;
import com.models.entities.Role;
import com.repositories.RoleRepository;
import com.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository repo;

    @Autowired
    private RoleMapper mapper;

    @Override
    public List<RoleDTO> getAllRoles() {
        List<RoleDTO> dtos = new ArrayList<>();

        for(Role r : repo.findAll()){
            dtos.add(mapper.mapToDTO(r));
        }

        return dtos;
    }

    @Override
    public RoleDTO findByName(String roleName) {
        return mapper.mapToDTO(repo.findByName(roleName));
    }
}
