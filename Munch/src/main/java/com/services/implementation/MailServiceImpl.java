package com.services.implementation;

import com.models.dto.GrouporderDTO;
import com.models.dto.ItemorderDTO;
import com.models.dto.RestaurantDTO;
import com.models.mail.MailFormat;
import com.services.ItemorderService;
import com.services.MailService;
import com.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

@Service
public class MailServiceImpl implements MailService {

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private ItemorderService itemorderService;

    @Autowired
    private OrderService orderService;

    public void emailTimer(long delay, RestaurantDTO restaurantDTO, GrouporderDTO grouporderDTO){
        Timer timer = new Timer("emailThread", true);

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(!orderService.findById(grouporderDTO.getId()).isSent()) {
                    sendMail(restaurantDTO, grouporderDTO);
                }else{
                    System.out.println("Already sent manually.");
                }
            }
        }, delay);
    }

    private void sendMail(RestaurantDTO restaurantDTO, GrouporderDTO grouporderDTO){

        double totalPrice = orderService.getTotalItemsSum(grouporderDTO);
        List<ItemorderDTO> items = itemorderService.findAllItemsByGroupOrder(grouporderDTO);

        MimeMessage message = emailSender.createMimeMessage();
        MailFormat mailFormat = new MailFormat(restaurantDTO, grouporderDTO, items, totalPrice);

        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, false);
            helper.setTo(restaurantDTO.getEmail());
            helper.setSubject("Order - Munchies");
            helper.setText(mailFormat.getEmailContent());

            if (mailFormat.isEmailReadyToSend()) {
                emailSender.send(message);

                grouporderDTO.setSent(true);
                orderService.saveOrUpdate(grouporderDTO, restaurantDTO);

                System.out.println("E-mail sent to restaurant " + restaurantDTO.getName() + ".");
            }else {
                System.out.println("Empty order,email not sent.");
            }
        }catch (MessagingException x){
            System.out.println("Error during sending email, email not sent.");
        }
    }




    //Nije po zahtevu,samo vezba da posaljem HTML template mejlom
    @Override
    public void sendAllRestaurants(List<RestaurantDTO> restList) {

        Context context = new Context();
        context.setVariable("restList", restList);

        MimeMessagePreparator predator = mimeMessage -> {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
            helper.setTo("apelanovic@gmail.com"); //TODO: treba trenutno ulogovanom korisniku
            helper.setSubject("Restaurants");
            helper.setFrom("order@munchies.com");
            helper.setText(templateEngine.process("restaurants_email_template", context), true);
        };

        emailSender.send(predator);
    }
}
