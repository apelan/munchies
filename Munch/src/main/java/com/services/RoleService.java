package com.services;

import com.models.dto.RoleDTO;
import com.models.entities.Role;

import java.util.List;

public interface RoleService {
    List<RoleDTO> getAllRoles();
    RoleDTO findByName(String roleName);
}
