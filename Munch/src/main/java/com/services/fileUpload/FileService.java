package com.services.fileUpload;

import com.models.dto.RestaurantDTO;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;

public interface FileService {
    RestaurantDTO uploadFile(MultipartFile multipartFile, RestaurantDTO restaurantDTO);
    void deleteFile(String fileName);
    String getFileExtension(Path path);
}
