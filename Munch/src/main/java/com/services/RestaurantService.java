package com.services;

import com.models.dto.RestaurantDTO;

import java.util.List;

public interface RestaurantService {
    RestaurantDTO saveOrUpdate(RestaurantDTO restaurant);
    void deleteRestaurantById(Integer id);
    void deleteRestaurantByUuid(String uuid);
    List<RestaurantDTO> findAllRestaurants();
    RestaurantDTO findRestaurantById(Integer id);
    RestaurantDTO findRestaurantByUuid(String uuid);
    RestaurantDTO getOne(Integer id);
    RestaurantDTO findByMenuLink(String menuLink);
}
