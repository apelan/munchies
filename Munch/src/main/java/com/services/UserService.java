package com.services;

import com.models.dto.UserDTO;

import java.util.List;

public interface UserService {
    UserDTO findUserByEmail(String email);
    List<UserDTO> findAllUsers();
    UserDTO saveOrUpdate(UserDTO u);
    void deleteUserById(Integer id);
    boolean checkIfPasswordMatches(String password, String confirmPassword);
}
