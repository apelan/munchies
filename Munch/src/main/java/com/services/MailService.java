package com.services;

import com.models.dto.GrouporderDTO;
import com.models.dto.RestaurantDTO;

import java.util.List;

public interface MailService {
    void sendAllRestaurants(List<RestaurantDTO> restList);
    void emailTimer(long delay,RestaurantDTO restaurantDTO, GrouporderDTO grouporderDTO);
}
