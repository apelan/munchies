package com.controllers;

import com.models.dto.RestaurantDTO;
import com.models.entities.Restaurant;
import com.services.MailService;
import com.services.OrderService;
import com.services.RestaurantService;
import com.services.fileUpload.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class RestaurantController {

    @Autowired
    private RestaurantService rest;

    @Autowired
    private FileService fileService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private MailService mailService;

    @GetMapping("/newRestaurant")
    public ModelAndView newRestaurant(){
        ModelAndView mav = new ModelAndView();
        Restaurant r = new Restaurant();
        mav.setViewName("rest_info");
        mav.addObject("restaurant", r);
        return mav;
    }

    @PostMapping("/saveRestaurant")
    public ModelAndView saveRestaurant(@Valid @ModelAttribute("restaurant") RestaurantDTO r, BindingResult result, @RequestParam("file") MultipartFile file){
        if(result.hasErrors()) {
            return new ModelAndView("rest_info");
        }

        r = fileService.uploadFile(file, r);
        r = rest.saveOrUpdate(r); //?

        return new ModelAndView("redirect:/restaurants");
    }

    @GetMapping("/deleteRestaurant/{uuid}")
    public ModelAndView deleteRestaurant(@PathVariable("uuid") String uuid){
        RestaurantDTO r = rest.findRestaurantByUuid(uuid);
        fileService.deleteFile(r.getMenuLink());
        rest.deleteRestaurantByUuid(uuid);
        return new ModelAndView("redirect:/restaurants");
    }

    @GetMapping("/showAllOrders/{uuid}")
    public ModelAndView showAllOrders(@PathVariable("uuid") String uuid){
        ModelAndView mav = new ModelAndView("show_all_orders");

        RestaurantDTO r = rest.findRestaurantByUuid(uuid);
        mav.addObject("restaurant", r);
        mav.addObject("ordersList", orderService.getActiveOrdersFromRestaurant(r.getId()));
        return mav;
    }

    @GetMapping("/sendAllRestaurantsEmail")
    public ModelAndView sendAllRestaurantsEmail(){
        List<RestaurantDTO> restList = rest.findAllRestaurants();

        mailService.sendAllRestaurants(restList);

        ModelAndView mav = new ModelAndView("restaurants");
        mav.addObject("restList", restList);
        return mav;
    }
}
