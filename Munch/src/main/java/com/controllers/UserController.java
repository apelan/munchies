package com.controllers;

import com.models.dto.RestaurantDTO;
import com.models.dto.UserDTO;
import com.services.RestaurantService;
import com.services.UserService;
import com.services.fileUpload.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Controller
public class UserController {

    @Value("${menu.path}")
    private String menuPath;

    @Autowired
    private RestaurantService rest;

    @Autowired
    private UserService userService;

    @Autowired
    private FileService fileService;

    @GetMapping({"/", "/restaurants"})
    public ModelAndView list(){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("restaurants");
        List<RestaurantDTO> restList = rest.findAllRestaurants();
        mav.addObject("restList", restList);
        return mav;
    }

    @GetMapping("/login")
    public ModelAndView login(){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("login");
        mav.addObject("registerSuccessful", false);
        return mav;
    }

    @GetMapping("/aboutRestaurant/{uuid}")
    public ModelAndView aboutRestaurant(@PathVariable("uuid") String uuid){
        RestaurantDTO r = rest.findRestaurantByUuid(uuid);
        ModelAndView mav = new ModelAndView();
        mav.addObject("restaurant", r);
        mav.setViewName("rest_info");
        return mav;
    }

    @GetMapping("/register")
    public ModelAndView register(){
        ModelAndView mav = new ModelAndView("register");
        UserDTO u = new UserDTO();
        mav.addObject("registerUser", u);
        mav.addObject("nomatch", false);
        return mav;
    }

    @PostMapping("/register")
    public ModelAndView saveUser(@Valid @ModelAttribute("registerUser") UserDTO user, BindingResult result){
        ModelAndView mav = new ModelAndView();

        if(result.hasErrors()){
            ModelAndView mavV = new ModelAndView();
            mavV.addObject("registerUser", user);
            mavV.setViewName("register");
            return mavV;
        }

        if(userService.checkIfPasswordMatches(user.getPassword(), user.getConfirmPassword())){
            user = userService.saveOrUpdate(user);
            mav.addObject("registerUser", user);
            mav.addObject("registerSuccessful", true);
            mav.setViewName("login");
            return mav;
        }else{
            mav.addObject("nomatch", true);
            mav.addObject("registerUser", user);
            mav.setViewName("register");
            return mav;
        }
    }

    @GetMapping("/file/{fileName}")
    public ResponseEntity<byte[]> showFile(@PathVariable("fileName") String fileName) throws IOException {
        HttpHeaders headers = new HttpHeaders();

        Path path = Paths.get(menuPath, fileName);

        byte[] fileBytes = Files.readAllBytes(path);

        if (Files.exists(path)){
            headers.add("content-disposition", "inline; filename=" + fileName);
            headers.setContentType(MediaType.parseMediaType(fileService.getFileExtension(path)));
        }
        return new ResponseEntity<>(fileBytes, headers, HttpStatus.OK);
    }
}