package com.controllers;

import com.models.dto.GrouporderDTO;
import com.models.dto.ItemorderDTO;
import com.models.dto.RestaurantDTO;
import com.services.ItemorderService;
import com.services.OrderService;
import com.services.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/employee")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private ItemorderService itemService;

    @GetMapping("/makeGroupOrder/{uuid}")
    public ModelAndView make(@PathVariable("uuid") String uuid){
        GrouporderDTO order = new GrouporderDTO();
        RestaurantDTO restaurant = restaurantService.findRestaurantByUuid(uuid);

        ModelAndView mav = new ModelAndView();
        mav.setViewName("new_group_order");
        mav.addObject("restaurant", restaurant);
        mav.addObject("order", order);
        return mav;
    }

    @PostMapping("/order/{uuid}")
    public ModelAndView makeGroupOrder(@Valid @ModelAttribute("order") GrouporderDTO order, BindingResult result, @PathVariable("uuid")String uuid){
        RestaurantDTO r = restaurantService.findRestaurantByUuid(uuid);

        if(result.hasErrors()){
            ModelAndView mav = new ModelAndView("new_group_order");
            mav.addObject("restaurant", r);
            mav.addObject("order", order);
            return mav;
        }

        GrouporderDTO newOrder = orderService.addGroupOrder(order, r.getId());

        //60 sekundi nakon isteka ordera on se salje automatski(ako prethodno nije manuelno poslat)
        long delay = Long.parseLong(newOrder.getTimeout()) * 60 * 1000 + 60000;
        orderService.setEmailTimer(delay, r, newOrder);

        RedirectView view = new RedirectView("/employee/order/" + r.getUuid() + "/" + newOrder.getUuid());
        view.setExposeModelAttributes(false);

        ModelAndView mav = new ModelAndView();
        mav.setView(view);
        mav.addObject("order", newOrder);
        mav.addObject("restaurant", r);
        mav.addObject("totalSum", orderService.getTotalItemsSum(newOrder));
        return mav;
    }

    @GetMapping("/order/{restaurantUuid}/{uuid}")
    public ModelAndView addItem(@PathVariable("uuid")String uuid, @PathVariable("restaurantUuid") String restaurantUuid){
        RestaurantDTO restaurantDTO = restaurantService.findRestaurantByUuid(restaurantUuid);
        GrouporderDTO order = orderService.findByUuid(uuid);

        ItemorderDTO item = new ItemorderDTO();
        item.setGroupOrder(order);

        List<ItemorderDTO> itemsDTO = itemService.findAllItemsByGroupOrder(order);

        ModelAndView mav = new ModelAndView("add_items_page");
        mav.addObject("restaurant", restaurantDTO);
        mav.addObject("order", order);
        mav.addObject("item", item);
        mav.addObject("itemsList", itemsDTO);
        mav.addObject("totalSum", orderService.getTotalItemsSum(order));
        return mav;
    }

    @PostMapping("/addNewItem/{restaurantUuid}/{uuid}")
    public ModelAndView addNewItem(@Valid @ModelAttribute("item") ItemorderDTO item, BindingResult result, @PathVariable("uuid") String uuid, @PathVariable("restaurantUuid") String restaurantUuid){
        GrouporderDTO grouporderDTO = orderService.findByUuid(uuid);

        if(result.hasErrors()){
            List<ItemorderDTO> itemsDTO = itemService.findAllItemsByGroupOrder(grouporderDTO);

            ModelAndView mav = new ModelAndView();
            mav.setViewName("add_items_page");
            mav.addObject("restaurant", restaurantService.findRestaurantByUuid(restaurantUuid));
            mav.addObject("item", item);
            mav.addObject("order", grouporderDTO);
            mav.addObject("itemsList", itemsDTO);
            mav.addObject("totalSum", orderService.getTotalItemsSum(grouporderDTO));
            return mav;
        }

        item = itemService.saveItemInGroup(item, grouporderDTO.getId());
        List<ItemorderDTO> itemsDTO = itemService.findAllItemsByGroupOrder(grouporderDTO);

        RedirectView view = new RedirectView("/employee/order/" + restaurantUuid + "/" + uuid);
        view.setExposeModelAttributes(false);

        ModelAndView mav = new ModelAndView();
        mav.setView(view);
        mav.addObject("restaurant", restaurantService.findRestaurantByUuid(restaurantUuid));
        mav.addObject("item", item);
        mav.addObject("order", grouporderDTO);
        mav.addObject("itemsList", itemsDTO);
        mav.addObject("totalSum", orderService.getTotalItemsSum(grouporderDTO));
        return mav;
    }

    @PostMapping("/sendEmail/{restaurantUuid}/{orderUuid}")
    public ModelAndView sendEmail(@PathVariable("restaurantUuid") String restaurantUuid, @PathVariable("orderUuid") String orderUuid){

        RestaurantDTO restaurantDTO = restaurantService.findRestaurantByUuid(restaurantUuid);
        GrouporderDTO order = orderService.findByUuid(orderUuid);

        List<RestaurantDTO> restList = restaurantService.findAllRestaurants();
        ModelAndView mav = new ModelAndView("redirect:/restaurants");
        mav.addObject("restList", restList);

        if(!order.isSent()) {
            orderService.setEmailTimer(1000, restaurantDTO, order);
        }
        return mav;
    }

}











