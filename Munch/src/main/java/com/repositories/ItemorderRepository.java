package com.repositories;

import com.models.entities.Itemorder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemorderRepository extends JpaRepository<Itemorder, Integer> {
}
