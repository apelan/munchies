package com.repositories;

import com.models.entities.Grouporder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Grouporder, Integer> {
    Grouporder findByUuid(String uuid);
}
