package com.repositories;

import com.models.entities.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RestaurantRepository extends JpaRepository<Restaurant, Integer> {
    Restaurant findByUuid(String uuid);
    Restaurant findByMenuLink(String menuLink);
}
