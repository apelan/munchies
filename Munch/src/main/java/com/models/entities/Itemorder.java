package com.models.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Itemorder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Basic
    @Column(name = "employee_name", nullable = false, length = 250)
    private String employeeName;

    @Basic
    @Column(name = "item_name", nullable = false, length = 250)
    private String itemName;

    @Basic
    @Column(name = "price", nullable = false, precision = 0)
    private double price;

    @ManyToOne
    @JoinColumn(name = "group_order", referencedColumnName = "id", nullable = false)
    private Grouporder groupOrder;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }


    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }


    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Itemorder itemorder = (Itemorder) o;
        return id == itemorder.id &&
                Double.compare(itemorder.price, price) == 0 &&
                Objects.equals(employeeName, itemorder.employeeName) &&
                Objects.equals(itemName, itemorder.itemName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, employeeName, itemName, price);
    }

    public Grouporder getGroupOrder() {
        return groupOrder;
    }

    public void setGroupOrder(Grouporder grouporderByOrder) {
        this.groupOrder = grouporderByOrder;
    }

    @Override
    public String toString() {
        return "Itemorder{" +
                "employeeName='" + employeeName + '\'' +
                ", itemName='" + itemName + '\'' +
                ", price=" + price +
                '}';
    }
}
