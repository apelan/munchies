package com.models.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Entity
public class Grouporder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Basic
    @Column(name = "uuid", nullable = false, length = 250)
    private String uuid;

    @Basic
    @Column(name = "creator", nullable = false, length = 250)
    private String creator;

    @Basic
    @Column(name = "timeout", nullable = false, length = 250)
    private String timeout;

    @ManyToOne
    @JoinColumn(name = "restaurant", referencedColumnName = "id", nullable = false)
    private Restaurant restaurant;

    @OneToMany(mappedBy = "groupOrder", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    private List<Itemorder> itemOrders;

    @Basic
    @Column(name = "time_created", nullable = false)
    private Timestamp timeCreated;

    @Basic
    @Column(name = "sent")
    private boolean sent;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }


    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public Timestamp getTimeCreated(){
        return timeCreated;
    }

    public void setTimeCreated(Timestamp timeCreated){
        this.timeCreated = timeCreated;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Grouporder that = (Grouporder) o;
        return id == that.id &&
                Objects.equals(creator, that.creator) &&
                Objects.equals(timeout, that.timeout);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, creator, timeout);
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurantByRestaurant) {
        this.restaurant = restaurantByRestaurant;
    }

    public List<Itemorder> getItemOrders() {
        return itemOrders;
    }

    public void setItemOrders(List<Itemorder> itemordersById) {
        this.itemOrders = itemordersById;
    }
}
