package com.models.entities;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Restaurant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Basic
    @Column(name = "uuid", nullable = false, length = 250)
    private String uuid;

    @Basic
    @Column(name = "name", nullable = false, length = 250)
    private String name;

    @Basic
    @Column(name = "address", nullable = false, length = 250)
    private String address;

    @Basic
    @Column(name = "phone_number", nullable = false, length = 250)
    private String phoneNumber;

    @Basic
    @Column(name = "menu_link", nullable = false, length = 500)
    private String menuLink;

    @Basic
    @Column(name = "delivery_info", nullable = false, length = 500)
    private String deliveryInfo;

    @Basic
    @Column(name = "email", nullable = false, length = 250)
    private String email;

    @OneToMany(mappedBy = "restaurant", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    private List<Grouporder> groupOrders;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMenuLink() {
        return menuLink;
    }

    public void setMenuLink(String menuLink) {
        this.menuLink = menuLink;
    }

    public String getDeliveryInfo() {
        return deliveryInfo;
    }

    public void setDeliveryInfo(String deliveryInfo) {
        this.deliveryInfo = deliveryInfo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Restaurant that = (Restaurant) o;
        return id == that.id &&
                Objects.equals(name, that.name) &&
                Objects.equals(address, that.address) &&
                Objects.equals(phoneNumber, that.phoneNumber) &&
                Objects.equals(menuLink, that.menuLink) &&
                Objects.equals(deliveryInfo, that.deliveryInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, address, phoneNumber, menuLink, deliveryInfo);
    }

    public List<Grouporder> getGroupOrders() {
        return groupOrders;
    }

    public void setGroupOrders(List<Grouporder> groupordersById) {
        this.groupOrders = groupordersById;
    }

    public String restaurantShortName(){
        return this.name.replaceAll(" ", "_");
    }
}
