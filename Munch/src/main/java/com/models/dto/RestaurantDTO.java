package com.models.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class RestaurantDTO {

    private int id;

    private String uuid;

    @Size(min = 1 , max = 250, message = "Restaurant name must be between 1 and 250 characters.")
    @NotNull
    @NotBlank(message = "Name must not be empty.")
    private String name;

    @Size(min = 1 , max = 250, message = "Address must be between 1 and 250 characters.")
    @NotNull
    @NotBlank(message = "Address must not be empty.")
    private String address;

    @Size(min = 1 , max = 250, message = "Phone number must be between 1 and 250 characters.")
    @NotNull
    @NotBlank(message = "Phone number must not be empty.")
    private String phoneNumber;

    private String menuLink;

    @Size(min = 1 , max = 500, message = "Delivery information must be between 1 and 500 characters.")
    @NotNull
    @NotBlank(message = "Delivery information must not be empty.")
    private String deliveryInfo;

    @NotNull
    @NotBlank(message = "Email must not be empty.")
    @Email
    private String email;

    private List<GrouporderDTO> groupOrders;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMenuLink() {
        return menuLink;
    }

    public void setMenuLink(String menuLink) {
        this.menuLink = menuLink;
    }

    public String getDeliveryInfo() {
        return deliveryInfo;
    }

    public void setDeliveryInfo(String deliveryInfo) {
        this.deliveryInfo = deliveryInfo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<GrouporderDTO> getGroupOrders() {
        return groupOrders;
    }

    public void setGroupOrders(List<GrouporderDTO> groupOrders) {
        this.groupOrders = groupOrders;
    }

    public String restaurantShortName(){
        return this.name.replaceAll(" ", "_");
    }
}
