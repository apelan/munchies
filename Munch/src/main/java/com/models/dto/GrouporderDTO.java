package com.models.dto;

import javax.validation.constraints.*;
import java.sql.Timestamp;

public class GrouporderDTO {
    private int id;

    private String uuid;

    @Size(min = 1 , max = 250, message = "Creator name must be between 1 and 250 characters.")
    @NotNull
    @NotBlank(message = "Creator name must not be empty.")
    private String creator;

    @Size(min = 1 , max = 60, message = "Timeout must be between 1 and 60 minutes.")
    @NotNull
    private String timeout;

    private Timestamp timeCreated;

    private boolean sent;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public Timestamp getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(Timestamp timeCreated) {
        this.timeCreated = timeCreated;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }
}
