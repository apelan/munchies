package com.models.dto;

import com.config.validation.ValidatePassword;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserDTO {

    private int id;

    @Size(min = 1 , max = 250, message = "First name must be between 1 and 250 characters.")
    @NotNull
    @NotBlank(message = "User first name must not be empty.")
    private String firstName;

    @Size(min = 1 , max = 250, message = "Last name must be between 1 and 250 characters.")
    @NotNull
    @NotBlank(message = "User last name must not be empty.")
    private String lastName;

    @Size(min = 1 , max = 250, message = "Email must be between 1 and 250 characters.")
    @NotNull
    @Email(message = "Email not valid.")
    private String email;

    @ValidatePassword
    private String password;

    @ValidatePassword
    private String confirmPassword;

    private RoleDTO role;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleDTO getRole() {
        return role;
    }

    public void setRole(RoleDTO role) {
        this.role = role;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
