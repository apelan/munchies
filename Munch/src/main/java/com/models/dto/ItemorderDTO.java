package com.models.dto;

import javax.validation.constraints.*;

public class ItemorderDTO {

    private int id;

    @Size(min = 1, max = 250, message = "Name must be between 1 and 250 characters.")
    @NotNull
    @NotBlank(message = "Name must not be empty.")
    private String employeeName;

    @Size(min = 1, max = 250, message = "Item name must be between 1 and 250 characters.")
    @NotNull
    @NotBlank(message = "Item name must not be empty.")
    private String itemName;

    @DecimalMax("5000.0")
    @DecimalMin("1.0")
    private double price;

    private GrouporderDTO groupOrder;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public GrouporderDTO getGroupOrder() {
        return groupOrder;
    }

    public void setGroupOrder(GrouporderDTO groupOrder) {
        this.groupOrder = groupOrder;
    }
}
