package com.models.mail;

import com.models.dto.GrouporderDTO;
import com.models.dto.ItemorderDTO;
import com.models.dto.RestaurantDTO;

import java.util.List;

public class MailFormat {
    private String mailHeader;
    private String mailFooter;
    private String mailContent;

    List<ItemorderDTO> items;

    public MailFormat(RestaurantDTO restaurantDTO, GrouporderDTO grouporderDTO, List<ItemorderDTO> items, double totalPrice) {
        this.items = items;
        mailHeader = "Hello restaurant " + restaurantDTO.getName() + ", there is new order for you.\n\n";
        mailFooter = "\n\nThanks for using our service, Munchies!";

        mailContent = "Order information: "
                + "\n Creator: " + grouporderDTO.getCreator()
                + "\n Time created: " + grouporderDTO.getTimeCreated()
                + "\n\nOrdered items: ";

        for (ItemorderDTO i : items) {
            mailContent += ("\nName: " + i.getItemName() + "\tEmployee name: " + i.getEmployeeName() + "\tPrice: " + i.getPrice());
        }

        mailContent += "\n\nTotal order price: " + totalPrice;
    }

    public void setMailContent(String mailContent) {
        this.mailContent = mailContent;
    }

    public String getEmailContent() {
        return mailHeader + mailContent + mailFooter;
    }

    public boolean isEmailReadyToSend() {
        return items.size() != 0;
    }
}
