package com.config.validation;

import org.passay.*;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

public class CustomPasswordValidator implements ConstraintValidator<ValidatePassword, String> {

    @Override
    public void initialize(ValidatePassword constraintAnnotation) {
    }

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        PasswordValidator validator = new PasswordValidator(Arrays.asList(
                new LengthRule(8, 20),
                new CharacterRule(EnglishCharacterData.UpperCase, 1),
                new CharacterRule(EnglishCharacterData.LowerCase, 1),
                new CharacterRule(EnglishCharacterData.Digit, 1),
                new WhitespaceRule()
        ));

        RuleResult result = validator.validate(new PasswordData(password));

        if (result.isValid()) {
            return true;
        }

        String template = "Password must include at least one upper case letter, one lower case letter, and one numeric digit and must have 8-20 characters without spaces.";
        context.buildConstraintViolationWithTemplate(template).addConstraintViolation().disableDefaultConstraintViolation();
        return false;
    }
}