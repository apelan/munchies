-- CREATE DATABASE IF NOT EXISTS munchtestdb;

CREATE TABLE `User` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(250) NOT NULL,
  `last_name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `role` int NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `Role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `Restaurant` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uuid` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `phone_number` varchar(250) NOT NULL,
  `menu_link` varchar(500) NOT NULL,
  `delivery_info` varchar(500) NOT NULL,
  `email` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `GroupOrder` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uuid` varchar(250) NOT NULL,
  `restaurant` int NOT NULL,
  `creator` varchar(250) NOT NULL,
  `timeout` varchar(250) NOT NULL,
  `sent` boolean NOT NULL,
  `time_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

CREATE TABLE `ItemOrder` (
  `id` int NOT NULL AUTO_INCREMENT,
  `employee_name` varchar(250) NOT NULL,
  `item_name` varchar(250) NOT NULL,
  `price` double NOT NULL,
  `group_order` int NOT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE `User` ADD CONSTRAINT `User_fk0` FOREIGN KEY (`role`) REFERENCES `Role`(`id`);

ALTER TABLE `GroupOrder` ADD CONSTRAINT `Order_fk0` FOREIGN KEY (`restaurant`) REFERENCES `Restaurant`(`id`);

ALTER TABLE `ItemOrder` ADD CONSTRAINT `ItemOrder_fk0` FOREIGN KEY (`group_order`) REFERENCES `GroupOrder`(`id`);

--Insert initial data (Admin pass: 123)
INSERT INTO Role (`name`) VALUES ('admin');
INSERT INTO User (`first_name`, `last_name`, `email`, `password`, `role`) VALUES ('AdminFirstName', 'AdminLastName', 'admin', '$2a$10$0uniLlgorbFSgSeen3OJ0e2d.ll4eVrpPecx4LcB2pngefhdknwzO', '1');

