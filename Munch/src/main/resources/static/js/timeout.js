//Copy
function copyFunction() {
    var copyText = document.getElementById("urlLink");
    copyText.select();
    document.execCommand("copy");
    alert("URL copied to clipboard.");
}

//Refresh table with items
setInterval( function () {
    var uuid = $("#orderUUID").val();
    var restaurantUuid = $("#restaurantUUID").val();
    $('#refresh').load("/employee/order/" + restaurantUuid + "/" + uuid + " #refresh");
}, 5000);

//Email
function emailSent() {
    $('#sendEmailButton').hide();
    $('#emailSentMsg').fadeIn(500);
    alert("Email will be sent shortly,be patient.");
}

//Timer countdown
var time = setInterval( function () {
    var uuid = $("#orderUUID").val();
    var restaurantUuid = $("#restaurantUUID").val();
    $("#refreshSentID").load("/employee/order/" + restaurantUuid + "/" + uuid + " #refreshSentID");

    var timeCreated = Number($("#timeCreated").val());
    var timeout = $("#timeouttime").val();

    var expireTime = timeCreated + (timeout * 60 * 1000);
    var currentTime = Date.now();

    var timeLeft = expireTime - currentTime;

    var mins = Math.floor((timeLeft % (1000 * 60 * 60)) / (1000 * 60));
    var secs = Math.floor((timeLeft % (1000 * 60)) / 1000);

    var sent = $("#orderSent").val();
    console.log(sent);

    /*

    mins < 10 == 00:10 mins > 10 == 11:10
    secs < 10 == 00:09 secs > 10 == 00:11

    mins < 10 && secs < 10 == 01:01
    mins > 10 && secs > 10 == 11:11

    */
    //Time out

    if(mins < 10 && secs < 10){
        document.getElementById("timerText").innerHTML = "Expire time: " + "0" + mins + ":0" + secs;
    }else{
        if(mins < 10){
            document.getElementById("timerText").innerHTML = "Expire time: " + "0" + mins + ":" + secs;
        }else if(secs < 10){
            document.getElementById("timerText").innerHTML = "Expire time: " + mins + ":0" + secs;
        }else{
            document.getElementById("timerText").innerHTML = "Expire time: " + mins + ":" + secs;
        }

    }
    if(timeLeft < 0){
        //clearInterval(time);
        document.getElementById("timerText").innerHTML = "This order is no longer available.";

        $("#addItemBtn").prop('disabled', true);

        //if email sent show message,if its not show button
        if(sent == "true"){
            $("#emailSentMsg").fadeIn(500);
            $("#sendEmailButton").hide();
        }else{
            $("#sendEmailButton").fadeIn(500);
        }
    }

}, 1000);

